import os
import base64
import subprocess
from time import sleep

from docker.errors import ContainerError

from bitbucket_pipes_toolkit.test import PipeTestCase


class ScpDeployTestCase(PipeTestCase):

    dirname = os.path.dirname(__file__)
    ssh_config_dir = '/opt/atlassian/pipelines/agent/ssh'

    ssh_key_file = 'identity'
    test_image_name = 'test-private-key'
    test_filename = 'test.txt'

    @classmethod
    def tearDownClass(cls):
        os.remove(os.path.join(cls.dirname, cls.ssh_key_file))
        os.remove(os.path.join(cls.dirname, f'{cls.ssh_key_file}.pub'))

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.LOCAL_PATH = 'test/'
        cls.REMOTE_PATH = '/tmp'

        dirname = os.path.dirname(__file__)

        subprocess.run(['ssh-keygen', '-f', os.path.join(
            dirname, cls.ssh_key_file), '-N', ''], check=False, text=True, capture_output=True)
        cls.private_key_image = cls.docker_client.images.build(
            path=dirname, dockerfile=os.path.join(dirname, 'Dockerfile'), tag=cls.test_image_name)

    def setUp(self):
        self.cwd = os.getcwd()
        self.api_client = self.docker_client.api
        self.ssh_key_file_container = self.docker_client.containers.run(
            self.test_image_name, detach=True)

        # wait until sshd is up
        for i in range(10):
            if b'listening' in self.ssh_key_file_container.logs():
                break
            sleep(1)
        else:
            raise Exception('Failed to start SSHD')

        self.container_ip = self.api_client.inspect_container(self.ssh_key_file_container.id)['NetworkSettings']['IPAddress']

        with open(os.path.join(os.path.dirname(__file__), self.ssh_key_file), 'rb') as identity_file:
            self.identity_content = identity_file.read()

        with open(os.path.join(self.LOCAL_PATH, self.test_filename), 'w') as test_file:
            test_file.write(f'Hello {self.ssh_key_file_container.short_id}')

    def tearDown(self):
        self.ssh_key_file_container.kill()
        testfile = os.path.join(self.dirname, self.test_filename)
        if os.path.exists(testfile):
            os.remove(testfile)

    def test_no_parameters(self):
        result = self.run_container()
        assert 'Validation errors' in result

    def test_default_success(self):
        try:
            result = self.run_container(
                environment={
                    'USER': 'root',
                    'SERVER': self.container_ip,
                    'SSH_KEY': base64.b64encode(self.identity_content),
                    'LOCAL_PATH': os.path.join(self.LOCAL_PATH, self.test_filename),
                    'REMOTE_PATH': self.REMOTE_PATH,
                    'EXTRA_ARGS_COUNT': 1,
                    'EXTRA_ARGS_0': '-o StrictHostKeyChecking=no',
                    'DEBUG': 'true'
                },
                volumes={
                    self.cwd: {'bind': self.cwd, 'mode': 'rw'},
                    self.ssh_config_dir: {'bind': self.ssh_config_dir, 'mode': 'rw'},
                }
            )

            assert self.test_filename in str(self.ssh_key_file_container.exec_run(f'ls {self.REMOTE_PATH}'))
            assert 'Deployment finished' in result

        except ContainerError as error:
            print(error.container.logs())
            assert False

    def test_with_wildcard_partially(self):
        try:
            result = self.run_container(
                environment={
                    'USER': 'root',
                    'SERVER': self.container_ip,
                    'SSH_KEY': base64.b64encode(self.identity_content),
                    'LOCAL_PATH': os.path.join(self.LOCAL_PATH, "*.txt"),
                    'REMOTE_PATH': self.REMOTE_PATH,
                    'EXTRA_ARGS_COUNT': 1,
                    'EXTRA_ARGS_0': '-o StrictHostKeyChecking=no',
                    'DEBUG': 'true'
                },
                volumes={
                    self.cwd: {'bind': self.cwd, 'mode': 'rw'},
                    self.ssh_config_dir: {'bind': self.ssh_config_dir, 'mode': 'rw'},
                }
            )

            assert self.test_filename in str(self.ssh_key_file_container.exec_run(f'ls {self.REMOTE_PATH}'))
            assert 'Deployment finished' in result

        except ContainerError as error:
            print(error.container.logs())
            assert False

    def test_fail_bad_user(self):
        try:
            result = self.run_container(
                environment={
                    'USER': 'bad_user',
                    'SERVER': self.container_ip,
                    'SSH_KEY': base64.b64encode(self.identity_content),
                    'LOCAL_PATH': os.path.join(self.LOCAL_PATH, "*.txt"),
                    'REMOTE_PATH': self.REMOTE_PATH,
                    'EXTRA_ARGS_COUNT': 1,
                    'EXTRA_ARGS_0': '-o StrictHostKeyChecking=no',
                    'DEBUG': 'true'
                },
                volumes={
                    self.cwd: {'bind': self.cwd, 'mode': 'rw'},
                    self.ssh_config_dir: {'bind': self.ssh_config_dir, 'mode': 'rw'},
                }
            )

            assert 'Permission denied' in result
            assert 'Deployment failed' in result

        except ContainerError as error:
            print(error.container.logs())
            assert False
