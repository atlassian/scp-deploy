from copy import copy
from unittest import mock, TestCase
import os
import sys


class ScpDeployTestCase(TestCase):
    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path

    @mock.patch.dict(os.environ, {
        'USER': 'user',
        'SERVER': 'my-ec2-host.com',
        'REMOTE_PATH': '/srv/',
        'LOCAL_PATH': 'test/tmp',
        'EXTRA_ARGS_COUNT': '2',
        'EXTRA_ARGS_0': '-o',
        'EXTRA_ARGS_1': 'ProxyCommand="ssh user@my-proxy.com nc my-ec2-host.com 22"',
    })
    @mock.patch('subprocess.run')
    @mock.patch('pipe.pipe.ScpDeployPipe.setup_ssh_config', mock.Mock())
    def test_scp_deploy_not_existing_path(self, subprocess_mock):
        from pipe.pipe import ScpDeployPipe, schema

        subprocess_mock.return_value = mock.Mock(returncode=0)
        with self.assertRaises(SystemExit):
            ScpDeployPipe(schema=schema, check_for_newer_version=True).run()

        subprocess_mock.assert_not_called()

    @mock.patch.dict(os.environ, {
        'USER': 'user',
        'SERVER': 'my-ec2-host.com',
        'REMOTE_PATH': '/srv/',
        'LOCAL_PATH': 'test/*.txt',
        'EXTRA_ARGS_COUNT': '2',
        'EXTRA_ARGS_0': '-o',
        'EXTRA_ARGS_1': 'ProxyCommand=\"ssh user@my-proxy.com nc main-host 22\"'
    })
    @mock.patch('subprocess.run')
    @mock.patch('pipe.pipe.ScpDeployPipe.setup_ssh_config', mock.Mock())
    def test_scp_deploy_extra_args_double_quotes_second(self, subprocess_mock):
        from pipe.pipe import ScpDeployPipe, schema

        subprocess_mock.return_value = mock.Mock(returncode=0)

        ScpDeployPipe(schema=schema, check_for_newer_version=True).run()
        subprocess_mock.assert_called_once_with(
            ['scp', '-rp', '-o',
             'ProxyCommand="ssh user@my-proxy.com nc main-host 22"',
             'test/requirements.txt', 'user@my-ec2-host.com:/srv/'])

    @mock.patch.dict(os.environ, {
        'USER': 'user',
        'SERVER': 'my-ec2-host.com',
        'REMOTE_PATH': '/srv/',
        'LOCAL_PATH': 'test',
        'EXTRA_ARGS_COUNT': '4',
        'EXTRA_ARGS_0': '-o',
        'EXTRA_ARGS_1': 'ControlPath="~/.mysecretfiles/master-%r@%h:%p"',
        'EXTRA_ARGS_2': '-o',
        'EXTRA_ARGS_3': 'ControlMaster=auto'
    })
    @mock.patch('subprocess.run')
    @mock.patch('pipe.pipe.ScpDeployPipe.setup_ssh_config', mock.Mock())
    def test_scp_deploy_extra_args_double_quotes_third(self, subprocess_mock):
        from pipe.pipe import ScpDeployPipe, schema

        subprocess_mock.return_value = mock.Mock(returncode=0)

        ScpDeployPipe(schema=schema, check_for_newer_version=True).run()

        subprocess_mock.assert_called_once_with(
            ['scp', '-rp',
             '-o', 'ControlPath="~/.mysecretfiles/master-%r@%h:%p"',
             '-o', 'ControlMaster=auto',
             'test', 'user@my-ec2-host.com:/srv/'])
