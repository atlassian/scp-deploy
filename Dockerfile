FROM python:3.10

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update && apt-get install --no-install-recommends -y openssh-client=1:9.* \
    sshpass=1.* && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /
COPY pipe /
COPY LICENSE.txt README.md pipe.yml /

RUN pip install --no-cache-dir -r requirements.txt

ENTRYPOINT ["python", "/pipe.py"]
