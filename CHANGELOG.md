# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.5.2

- patch: Update the Readme. Fix variables yaml schema definition.

## 1.5.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 1.5.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.4.2

- patch: Internal maintainance: Refactor tests.

## 1.4.1

- patch: Update a link to the guide for multiple SSH keys usage.

## 1.4.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.3.0

- minor: Internal maintenance: Update bitbucket-pipes-toolkit version to 4.0.0. Update packages in test/requirements.txt.
- minor: Internal maintenance: Update docker image in Dockerfile to python:3.10.
- patch: Internal maintenance: Update community link.
- patch: Internal maintenance: Update docker image and pipes versions in pipelines configuration file and README.

## 1.2.1

- patch: Internal maintenance: Add required tags for test infra resources.

## 1.2.0

- minor: Provide advanced example with adding PROXY_COMMAND for complex quotes.
- minor: Update dependencies for openssh-client, sshpass in Dockerfile.

## 1.1.0

- minor: Bump pipes-toolkit -> 2.2.0.

## 1.0.1

- patch: Bugfix: support wildcard in LOCAL_PATH variable.

## 1.0.0

- major: Changed EXTRA_ARGS parameter from string to array type in order to support better advanced use cases.
- minor: Moved scp-deploy pipe to Python for better maintainability and testability.

## 0.3.13

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 0.3.12

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.3.11

- patch: Internal maintenance: Add gitignore secrets.

## 0.3.10

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.

## 0.3.9

- patch: Update the Readme with details about LOCAL_PATH parameter.

## 0.3.8

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.3.7

- patch: Improve cloud formation stack template

## 0.3.6

- patch: Update README.md with modifying permissions example

## 0.3.5

- patch: Internal maintenance: Add auto infrastructure for tests.

## 0.3.4

- patch: Update pipes bash toolkit version.

## 0.3.3

- patch: Updated contributing guidelines

## 0.3.2

- patch: FIX issue with large writes to stdout failing with 'Resource temporarily unavailable'

## 0.3.1

- patch: Standardising README and pipes.yml.

## 0.3.0

- minor: Force the git ssh command used when pushing a new tag to override the default SSH configuration for piplines.

## 0.2.0

- minor: Adopt new naming and consistency conventions.

## 0.1.2

- patch: Use quotes for all pipes examples in README.md.

## 0.1.1

- patch: Restructure README.md to match user flow.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines SFTP pipe.
