# Bitbucket Pipelines Pipe: SCP deploy

Deploy files and directories using SCP.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/scp-deploy:1.5.2
  variables:
    USER: '<string>'
    SERVER: '<string>'
    REMOTE_PATH: '<string>'
    LOCAL_PATH: '<string>'
    # SSH_KEY: '<string>' # Optional.
    # EXTRA_ARGS: '<array>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable        | Usage                                                                                                                                                                                                                                                                                                                                                                     |
|-----------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| SERVER (*)      | The remote host to transfer the files to.                                                                                                                                                                                                                                                                                                                                 |
| USER (*)        | The user on the remote host to connect as.                                                                                                                                                                                                                                                                                                                                |
| REMOTE_PATH (*) | The remote path to deploy files to.                                                                                                                                                                                                                                                                                                                                       |
| LOCAL_PATH (*)  | The local path to file or folder to be deployed.                                                                                                                                                                                                                                                                                                                          |
| SSH_KEY         | An alternate SSH_KEY to use instead of the key configured in the Bitbucket Pipelines admin screens (which is used by default). This should be encoded as per the instructions given in the docs for [using multiple ssh keys](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html#UseSSHkeysinBitbucketPipelines-multiple_keys) |
| EXTRA_ARGS      | Additional arguments passed to the scp command. Type: list. Default: empty list. (see [SCP docs][SCP docs] for more details).                                                                                                                                                                                                                                             |
| DEBUG           | Turn on extra debug information. Default: `false`.                                                                                                                                                                                                                                                                                                                        |

_(*) = required variable._

## Details

SCP copies files and directories between your local system to another server. It uses SSL for data transfer, which uses the same authentication
and provides the same security as SSH. It may also use many other features of SSH, such as compression.

More details: [scp man page][SCP docs]

By default, the SCP pipe will automatically use your configured SSH key and known_hosts file configured from the [Bitbucket Pipelines administration pages][using ssh keys].
You can pass the `SSH_KEY` parameter to use an alternate SSH key as per the instructions in the docs for
[using multiple ssh keys][using multiple ssh keys]

Note: you can use file globbing in the LOCAL_PATH to specify the files you want to deploy. Follow "Example how to deploy only files from build directory".

You can use $BITBUCKET_CLONE_DIR [default variable][default variable] - the absolute path of the directory that the repository is cloned into within the Docker container:

```
LOCAL_PATH: '${BITBUCKET_CLONE_DIR}'     # copy the current directory and all included files. Keeps the parent directory structure.
LOCAL_PATH: '${BITBUCKET_CLONE_DIR}/*'    # copy the contents from the current directory.
```

Or specify your "custom/local/path":

```
LOCAL_PATH: 'custom/local/path'    # copy your custom directory and all included files. Keeps the parent directory structure.
LOCAL_PATH: 'custom/local/path/*'   # copy the contents from your custom directory.
```

Keep in mind that if you specify the “/” (slash) as the LOCAL_PATH variable, it will copy all files from the root build container filesystem. So, we normally recommend using a relative directory custom/local/path or ${BITBUCKET_CLONE_DIR}/* if you want to copy all files from the build directory.


## Prerequisites
* If you want to use the default behaviour for using the configured SSH key and known hosts file, you must have configured 
  the [SSH private key and known_hosts][using ssh keys] to be used for the SCP pipe in your Pipelines settings.
 

## Examples

### Basic example:

Example how to deploy only files from your repository, i.e. doesn’t preserve the parent directory.

```yaml
script:
  - pipe: atlassian/scp-deploy:1.5.2
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/build/'
      LOCAL_PATH: '${BITBUCKET_CLONE_DIR}/*'
```


Example how to deploy only files from the alternative directory, i.e. "build".

```yaml
script:
  - pipe: atlassian/scp-deploy:1.5.2
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/build/'
      LOCAL_PATH: 'build/*'
```

### Advanced examples:
Here we pass extra arguments to the scp command to use port 8022, and enable extra debugging.

```yaml
script:
  - pipe: atlassian/scp-deploy:1.5.2
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/build/'
      LOCAL_PATH: 'build/'
      DEBUG: 'true'
      EXTRA_ARGS: ["-P", "8022"]
```

Example with alternate SSH key.

```yaml
script:
  - pipe: atlassian/scp-deploy:1.5.2
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/build/'
      LOCAL_PATH: 'build'
      SSH_KEY: $MY_SSH_KEY
      DEBUG: 'true'
      EXTRA_ARGS: ['-o', 'ServerAliveInterval=10']
```

Example with modifying targets permissions.

The SCP pipe **preserves modification** times, access times, and modes from the original file.

You can use the [ssh-run pipe][ssh-run pipe] to modify the target permissions on the remote server. Here is an example:

```yaml
script:
  - pipe: atlassian/scp-deploy:1.5.2
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/build/'
      LOCAL_PATH: '${BITBUCKET_CLONE_DIR}/*'
  - pipe: atlassian/ssh-run:0.4.3
    variables:
      SSH_USER: 'ec2-user'
      SERVER: '127.0.0.1'
      COMMAND: 'chmod -R 755 /var/www/build'
```

Example with adding PROXY_COMMAND for complex quotes.

```yaml
- step:
  name: Your step name
  script:
  - PROXY_COMMAND="ProxyCommand=\"ssh <your_user_here>@<your_ip_or_host_here> nc <final_destination> 22\""
  - pipe: atlassian/scp-deploy:1.5.2
    variables:
      LOCAL_PATH: 'your_file'
      REMOTE_PATH: 'your_path'
      SERVER: '<your_final_server>'
      USER: 'your_user'
      EXTRA_ARGS: ['-o', $PROXY_COMMAND]
      DEBUG: 'true'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,scp
[default variable]: https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html
[SCP docs]: http://manpages.ubuntu.com/manpages/trusty/en/man1/scp.1.html
[using ssh keys]: https://support.atlassian.com/bitbucket-cloud/docs/using-ssh-keys-in-bitbucket-pipelines/
[using multiple ssh keys]: https://support.atlassian.com/bitbucket-cloud/docs/use-multiple-ssh-keys-in-your-pipeline/
[ssh-run pipe]: https://bitbucket.org/atlassian/ssh-run/
